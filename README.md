Nemoscripter

The basic idea for this project is to provide a single file python script that can be imported in your Nemo Scripts 
to handle dealing with the environment variables and to provide some error checking for file names and paths.  The goal
is to provide a (hopefully) friendly interface for writing quick scripts to automate file manager tasks without having 
to reimplement the basics in every script.  I've begun to add compatibility to some other file managers (Caja, Nautilus),
but this is untested so far.