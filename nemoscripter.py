#!/usr/bin/python
__copyright__= "Copyright 2014, Brian Perch bcp1551@gmail.com"
__copying__= '''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
__version__= 0.02

import os, sys
import subprocess

#Nemo File Manager (Cinnamon)
__ENVIRO_VAR__ = 'NEMO_SCRIPT_SELECTED_FILE_PATHS'
#Nautilus File Manager (Gnome)
#__ENVIRO_VAR__ = 'NAUTILUS_SCRIPT_SELECTED_FILE_PATHS'
#Caja File Manager (Mate)
#__ENVIRO_VAR__ = 'CAJA_SCRIPT_SELECTED_FILE_PATHS'
#testing: run command 'export your_env_var=<some paths>'
#__ENVIRO_VAR__= 'your_env_var'
__TERMINAL__='gnome-terminal -e'

class NemoScripter(object):
    '''This is a class that encapsulates useful functionality for writing
       and testing file manager scripts.'''
    
    def __init__(self, cmd_string):
	"""Constructor"""
        self.command = cmd_string
        self.filelist = self.__setup_filelist__()
	self.log('constructor complete')
	
        
    def __setup_filelist__(self):
	'''checks for the environment variable __ENVIRO_VAR__ and converts it
	   to a Python list of single quoted file paths.  This will fail and
	   log error if any file path contains embedded single quotes. This
	   will fail and log error if environment varible is not found.'''
	   
        if os.environ.has_key(__ENVIRO_VAR__):
	    self.log('Environment variable found')
	    file_manager_files = os.environ.get(__ENVIRO_VAR__)
	    split_list = file_manager_files.splitlines()
	    if "'" in split_list:
		self.log('embedded single quote found, exiting')
		sys.exit()
	    else:
		final_list = []
		for item in split_list:
		    final_list.append("'" + item + "'")
		self.log(str(final_list))    
		return final_list
	else:
	    self.log('no enviroment variable found, exiting')
	    sys.exit()
	    
    def run(self):
	'''Executes cmd_string and each file path selected.  Ex. ls -l /home'''
	for item in self.filelist:
	    self.log('shell cmd -----> ' + self.command +  " " + item)
	    #subprocess.Popen(self.command +  " " + item , shell = True)
	    
    def log(self, entry):
	'''Opens /tmp/log.txt and appends the entry + a newline.'''
	f = file("/tmp/log.txt", 'a')
	f.write(entry + '\n')
	f.close()
    
    def view_log(self):
	'''Executes: __TERMINAL__ nano /tmp/log.txt'''
	subprocess.Popen((__TERMINAL__ + ' "nano /tmp/log.txt"'), shell=True)
	
    
#test run	
a = NemoScripter('ls')
a.run()
a.view_log()